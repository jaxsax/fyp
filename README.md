# Introduction

This repository provides a bunch of tools to process satellite images for Sentinel-2 downloaded from [SentinelHub](https://scihub.copernicus.eu/dhus/#/home).

These processing steps prepare it for manual model classification via QGIS and further on, classification through the shapefiles exported from QGIS via the SCP plugin.

# Installing the environment

This project works best via Anaconda/Miniconda, but you can use others by installing the dependencies found in dependencies.yml.
It is assumed that you are working in Linux or any compatible environments (we use shell scripts).

> conda env create -n s2 -f dependencies.yml

Aside from python dependencies, you'll also need [Sen2Cor](http://step.esa.int/main/third-party-plugins-2/sen2cor/). This provides tools to do atmospheric correction.

```sh
# Once you have downloaded the library, install it to the vendor/sen2cor directory like so:

./Sen2Cor-02.05.05-Linux64.run --target <PROJECT_DIRECTORY>/vendor/sen2cor
```

# Outline

1. [Download images and prepare](#downloading-the-images)

2. [Manual classification](#manual-classification)

3. [Model training](#model-training)

# Downloading the images

Once you have downloaded the archives from scihub, place them all in a folder and run prepare.py

prepare.py does the following:

1. Extract all archives into a temporary working folder

2. Runs Sen2Cor for atmospheric correction

3. Runs a reprojection on all jp2 files to match the resolution of Band 4 (10980x10980) or 10m resolution

4. Writes all jp2 files to GTiff files

# Manual classification

In the event where you want more data to train with or can't find any, you can manually classify with [QGIS](https://www.qgis.org/en/site/) and [SCP](https://semiautomaticclassificationmanual-v5.readthedocs.io/en/latest/)

## Useful links for more data

You have to gague for yourself whether this is really useful or not, I didn't use these but thought it may come in handy in the future.

1. http://madm.dfki.de/downloads
2. https://www.kaggle.com/c/dstl-satellite-imagery-feature-detection
3. https://spacenetchallenge.github.io/datasets/datasetHomePage.html

# Model training

At this moment, the pipeline expects features to be in the form of shp files (ESRI Shapefile). Once you have that, you may run a shell script to extract features out from the shp and tif files into a single npy file.

Example:
```sh

./convert-classified.sh <location of source image folder; all the different bands> <a name for this particular set of classification>
```

Once feature extraction is complete, I like to copy the resulting npy files into one folder and then run **train.sh** on that folder. That script contains all the models that are used for classification training namely:

1. A baseline classifier that predicts the most popular prediction (based off features) [more info](https://machinelearningmastery.com/implement-baseline-machine-learning-algorithms-scratch-python/)

2. A random forest classifier

All the classifier scripts expect an `inputs.json` file similar to the following:

```json
{
	"some_training_input": {
		"X": "X_some_training_input.npy",
		"y": "y_some_training_input.npy"
	}
}
```

This is the exact format that `convert-classified.sh` generates based off naming conventions if you copy it from the features folder.

Once training is complete, the RF model outputs a pkl file in the root of the project that you **must** move to a models folder. If you used different parameters in the training, it would likely be that `classify.sh` is unable to find the model and so you must update accordingly.

# Image classification

For this stage, just run `classify.sh` with a source image folder and an output folder to dump the classified information

Example:
```sh
./classify.sh <location of source image with all bands> classification
```
