#!/usr/bin/env python

from pathlib import Path

import argparse as ap
import rasterio as rio
import numpy as np
import matplotlib.pyplot as plt

from utils import create_band_sorter, get_files_matching
from rasterio.crs import CRS
from rasterio.warp import reproject, Resampling

def main():
    parser = ap.ArgumentParser(description="Analysis on ground truth imagery")
    parser.add_argument('path_to_jp2',
                        help='Path to directory that contains jp2 images')
    parser.add_argument('path_to_shp',
                        help='Path to shp file location')
    parser.add_argument('-e', '--extension',
                        help="Source image extension",
                        default=".jp2")
    args = parser.parse_args()
    bands = "B01,B02,B03,B04,B05,B06,B07,B08,B8A,B09,B10,B11,B12".split(",")
    band_sorter = create_band_sorter(bands, args.extension)

    images_directory = Path(args.path_to_jp2)
    jp2_files = get_files_matching(images_directory, bands, args.extension)
    jp2_files = sorted(jp2_files, key=band_sorter)

    np_save_file = Path(f"{Path(jp2_files[0]).stem[:-4]}-{'-'.join(bands)}.npy")
    if np_save_file.is_file():
        print("Reloading previously created save file")
        bands = np.load(np_save_file)
    else:
        # Putting None so we can index normally
        bands = [None]
        with rio.open(images_directory.joinpath(jp2_files[3])) as src:
            reference = src.profile
            reference_shape = (src.width, src.height)

        for jp2 in jp2_files:
            print(f"Reading {jp2}")
            with rio.open(images_directory.joinpath(jp2)) as src:
                destination = np.zeros(reference_shape)
                reproject(
                    source=rio.band(src, 1),
                    destination=destination,
                    src_transform=src.transform,
                    src_crs=src.crs,
                    src_nodata=src.nodata,
                    dst_transform=reference['transform'],
                    dst_crs=reference['crs'],
                    dst_nodata=reference['nodata'],
                    resampling=Resampling.bilinear)
                bands.append(destination)

        bands = np.array(bands)
        np.save(np_save_file, bands)

    print(bands.shape)


def band_vari(bands):
    # Visible Atmospherically Resistent Index (Green - Red) / (Green + Red - Blue)
    return bands[3] - bands[4] / (bands[3] + bands[4] - bands[2])

def band_math_ratio(bands, k, i):
    bk = bands[k]
    bi = bands[i]
    vi = (bk - bi) / (bk + bi)

    return vi

if __name__ == '__main__':
    main()
