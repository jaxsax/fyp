#!/usr/bin/env sh

# Input source (some clipped image)
# Generate stacked image (use the py file to generate this)
# classify on the stacked image

SOURCE_FOLDER=$1
STACKED_OUT_FOLDER=$2
STACKED_IMAGE_PATH="${STACKED_OUT_FOLDER}/STACKED.tif"
GROUNDTRUTH_IMAGE_PATH=$3
MODELS_FOLDER=models

mkdir -p $STACKED_OUT_FOLDER

# ================ RANDOM FOREST =================
python generate_stacked.py $SOURCE_FOLDER  $STACKED_IMAGE_PATH
python convert_groundtruth_rgb.py $GROUNDTRUTH_IMAGE_PATH $STACKED_OUT_FOLDER
python classify_rf.py $STACKED_IMAGE_PATH $GROUNDTRUTH_IMAGE_PATH \
       $MODELS_FOLDER/rf.pkl.compressed \
       $STACKED_OUT_FOLDER
python classify_kmeans.py $STACKED_IMAGE_PATH $GROUNDTRUTH_IMAGE_PATH \
       $MODELS_FOLDER/kmeans.pkl.compressed \
       $STACKED_OUT_FOLDER
