#!/usr/bin/env python

from pathlib import Path
from sklearn.externals import joblib
from rasterio.plot import reshape_as_image, reshape_as_raster, show

import numpy as np
import pandas as pd
import rasterio as rio
import sklearn.metrics as metrics
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import argparse as ap

import sys
import collections
import logging

logging.basicConfig(level=logging.INFO,
                    format="%(asctime)s [%(name)s] [%(levelname)s] %(message)s")

logger = logging.getLogger('classify_rf')

def main():
    parser = ap.ArgumentParser(description="Classifies an image using random forest")
    parser.add_argument('stacked_image',
                        type=str,
                        help='Path to stacked image')
    parser.add_argument('ground_truth',
                        type=str,
                        help='Path to single band ground truth image where the number is its label id')
    parser.add_argument('rf_file',
                        type=str,
                        help='Path to random forest pkl')
    parser.add_argument('rf_output_directory',
                        type=str,
                        help='Path to dump outputs')
    parser.add_argument('--show',
                        type=bool,
                        help='To show images inline or not')
    args = parser.parse_args()

    stacked_image = Path(args.stacked_image)
    ground_truth_image = Path(args.ground_truth)
    output_directory = Path(args.rf_output_directory)
    output_directory.mkdir(exist_ok=True)
    pkl_file = Path(args.rf_file)

    rf = joblib.load(pkl_file)

    with rio.open(stacked_image) as src:
        img = src.read()
        count=src.count
        source_profile=src.profile

    reshaped_image = reshape_as_image(img)
    pred_source = reshaped_image.reshape((-1, count))
    pred = rf.predict(pred_source).astype(np.uint8)
    n = int(np.max(pred))

    colors = dict({
        (0, (0, 0, 0, 255)),
        (1, (0, 120, 215, 255)), # Water
        (3, (255, 85, 0, 255)),   # Man made surfaces
        (4, (3, 254, 0, 255)), # Agricultural
        (5, (255, 0, 249)), # Mountains
        (6, (84, 170, 0, 255)) # Forests
    })
    labels = dict({
        (0, "Unclassified"),
        (1, "Water"),
        (3, "Man-made"),
        (4, "Agricultural"),
        (5, "Mountains"),
        (6, "Forests")
    })

    unknown_color = (1.0, 1.0, 1.0, 0.0)

    for k in colors:
        v = colors[k]
        _v = [_v / 255.0 for _v in v]
        colors[k] = _v

    index_colors = [colors[key] if key in colors
                    else unknown_color for key in range(1, n+1)]
    patches = [ mpatches.Patch(color=v, label=labels[k]) for k, v in colors.items()]

    logger.info(np.unique(pred))
    logger.info(collections.Counter(pred))

    mapR = np.vectorize(lambda x: colors[x][0])
    mapG = np.vectorize(lambda x: colors[x][1])
    mapB = np.vectorize(lambda x: colors[x][2])

    reshaped = pred.reshape(img[0,:,:].shape)
    reshaped_rgb = np.array([mapR(reshaped), mapG(reshaped), mapB(reshaped)])

    data = 255 * reshaped.reshape((1, reshaped.shape[0], reshaped.shape[1]))
    data_rgb = (255 * reshaped_rgb).astype(rio.uint8)

    source_profile.update(count=3)
    with rio.open(output_directory.joinpath("rf_classified.RGB.tif"), "w", **source_profile) as dst:
        dst.write(data_rgb)

    source_profile.update(count=1)
    with rio.open(output_directory.joinpath("rf_classified.tif"), "w", **source_profile) as dst:
        dst.write(data)

    with rio.open(ground_truth_image) as src:
        gt = src.read(1)

    gt = gt[:,~np.all(gt == 0, axis=0)]
    gt = gt[:,~np.all(gt == 255, axis=0)]
    gt_reshaped = gt.reshape(-1)

    jacc = metrics.jaccard_similarity_score(gt_reshaped, pred)
    kappa = metrics.cohen_kappa_score(gt_reshaped, pred)
    logger.info("Jaccard score: %.2f", jacc)
    logger.info("Kappa coefficient: %.2f", kappa)

    show(data_rgb)

    if args.show:
        fig, axs = plt.subplots(1,3,figsize=(15,15))
        axs[0].imshow(reshaped_image[:,:,[2,1,0]])
        show(data_rgb, ax=axs[1], title='classified image')
        axs[1].legend(handles=patches)
        show(data, ax=axs[2], title='single band classified')
        plt.show()
if __name__ == '__main__':
    main()
