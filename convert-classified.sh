#!/usr/bin/env bash

# This script requires as input, a folder specifying source images and a folder specifying classified outputs
# With the info, it runs generate_stacked.py and extract_features.py into an output folder

# Steps
# 1. Generate stacked image
# 2. Extract features into npy file

if [ $# -lt 2 ]; then
    echo "usage: source_folder classified_folder";
    exit 1;
fi

SOURCE_IMAGE_FOLDER=$1
FEATURE_EXTRACTION_FOLDER="working/feature_extraction/$2"
CLASSIFIED_FILE="${FEATURE_EXTRACTION_FOLDER}/STACKED.tif"
OUTPUT_FOLDER="${FEATURE_EXTRACTION_FOLDER}/features"

mkdir -p $FEATURE_EXTRACTION_FOLDER
mkdir -p $OUTPUT_FOLDER

python generate_stacked.py $SOURCE_IMAGE_FOLDER $CLASSIFIED_FILE
python extract_features.py $FEATURE_EXTRACTION_FOLDER $OUTPUT_FOLDER
