import rasterio
import zipfile
import os.path
import sys
from pathlib import Path
import numpy as np
import affine
from rasterio.crs import CRS
from rasterio.warp import reproject, Resampling

def get_immediate_directories(directory):
    return [x for x in directory.iterdir() if x.is_dir()]

def main(argv):
    if len(argv) < 2:
        print("Need input zip")
        sys.exit(1)

    source_zip = Path(argv[1])
    target_directory = Path("target").resolve()
    target_success_marker = target_directory.joinpath("_SUCCESS")
    source_folder_name = target_directory.joinpath(f"{source_zip.stem}.SAFE")
    processing_directory = Path("processing").resolve()

    print(f"source zip: {source_zip}")
    print(f"target_directory: {target_directory}")
    print(f"extracted_folder: {source_folder_name}")

    if not source_zip.exists():
        print(f"Not a zip! '{source_zip}'")
        sys.exit(1)

    if not target_success_marker.exists():
        print("Success marker doesn't exist, extracting file")

        print("Attempting to extract zip")
        with zipfile.ZipFile(source_zip, "r") as zip_ref:
            zip_ref.extractall(target_directory)
            target_success_marker.touch()

    granule_directory  = source_folder_name.joinpath("GRANULE")
    warp_files = True
    write_warped = True
    wanted_bands = ["B02", "B03", "B04", "B05", "B06", "B07", "B08", "B8A", "B11", "B12"]
    for granule in get_immediate_directories(granule_directory):
        granule_source_directory = granule_directory.joinpath(granule, "IMG_DATA")
        jp2_files = [x for x in granule_source_directory.iterdir()
                     if any([True if b in x.name else False for b in wanted_bands])
                     and x.name.endswith(".jp2")]
        jp2_files.sort()

        print(f"Stacking {len(jp2_files)} bands")

        # warp all sizes to be the same and write back to the same .jp2
        reference_jp2 = [x for x in jp2_files if "B04" in x.name]
        if len(reference_jp2) == 0:
            print(":( can't find jp2 to reference")
            sys.exit(1)
        reference_jp2 = reference_jp2[0]

        with rasterio.open(reference_jp2) as src:
            reference_profile = src.profile
        reference_profile.update(driver='GTiff')
        print(reference_profile)

        warp_directory = processing_directory.joinpath("warp")
        warp_directory.mkdir(exist_ok=True)

        if warp_files:
            for f in jp2_files:
                print(f"Processing {f.name}")
                warp_out = warp_directory.joinpath(f"{f.stem}.tif")
                with rasterio.open(f) as src:
                    with rasterio.open(warp_out, 'w', **reference_profile) as dst:
                        reproject(
                            source=rasterio.band(src, 1),
                            destination=rasterio.band(dst, 1),
                            src_transform=src.transform,
                            src_crs=src.crs,
                            src_nodata=src.nodata,
                            dst_transform=reference_profile['transform'],
                            dst_crs=reference_profile['crs'],
                            dst_nodata=reference_profile['nodata'],
                            resampling=Resampling.bilinear)
        if write_warped:
            warped_files = [x for x in warp_directory.iterdir() if x.name.endswith(".tif")]
            warped_files.sort()

            stack_profile = reference_profile
            stack_profile.update(count=len(warped_files))

            granule_output_gtiff = processing_directory.joinpath(f"{granule.name}.tif")
            with rasterio.open(granule_output_gtiff, 'w', **stack_profile) as dst:
                for id, layer in enumerate(warped_files, start=1):
                    print(f"Writing id={id} layer={layer}")
                    with rasterio.open(layer) as src:
                        dst.write_band(id, src.read(1))


if __name__ == '__main__':
    main(sys.argv)
