#!/usr/bin/env python

from pathlib import Path

import numpy as np
import rasterio as rio

import sys

def main():
    ground_truth = Path(sys.argv[1])
    output_directory = Path(sys.argv[2])

    colors = dict({
        (0, (0, 0, 0, 255)),
        (1, (0, 120, 215, 255)), # Water
        (3, (255, 85, 0, 255)),   # Man made surfaces
        (4, (3, 254, 0, 255)), # Agricultural
        (5, (255, 0, 249)), # Mountains
        (6, (84, 170, 0, 255)) # Forests
    })

    for k in colors:
        v = colors[k]
        _v = [_v / 255.0 for _v in v]
        colors[k] = _v

    with rio.open(ground_truth) as src:
        data = src.read(1)
        source_profile = src.profile

    mapR = np.vectorize(lambda x: colors[x][0])
    mapG = np.vectorize(lambda x: colors[x][1])
    mapB = np.vectorize(lambda x: colors[x][2])

    data_rgb = np.array([mapR(data), mapG(data), mapB(data)])
    data_rgb = (255 * data_rgb).astype(rio.uint8)

    output_file = output_directory.joinpath(f"{ground_truth.stem}.RGB.tif")
    source_profile.update(count=3)
    source_profile.update(dtype=rio.uint8)
    source_profile.update(nodata=0)
    with rio.open(output_file, 'w', **source_profile) as dst:
        dst.write(data_rgb)

if __name__ == '__main__':
    main()
