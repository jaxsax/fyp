#!/usr/bin/env python

from pathlib import Path
from sklearn.model_selection import train_test_split

import tensorflow as tf
import matplotlib.pyplot as plt

import argparse as ap
import numpy as np

import json
import logging
import keras

logging.basicConfig(level=logging.INFO,
                    format="%(asctime)s [%(name)s] [%(levelname)s] %(message)s")

logger = logging.getLogger('train_nn')

def simple_model():

    model = keras.Sequential([
        keras.layers.Flatten(input_shape=(28,28)),
        keras.layers.Dense(128, activation=tf.nn.relu),
        keras.layers.Dense(10, activation=tf.nn.softmax)
    ])

    model.compile(optimizer='adam',
                loss='sparse_categorical_crossentropy',
                metrics=['accuracy'])

    return model

def cnn(n):
    model = keras.Sequential([
        keras.layers.Conv2D(32, kernel_size=(3, 3),
                            activation=tf.nn.relu,
                            input_shape=(28, 28, 1)),
        keras.layers.Conv2D(64, (3,3), activation=tf.nn.relu),
        keras.layers.MaxPooling2D(pool_size=(2, 2)),
        keras.layers.Dropout(0.25),
        keras.layers.Flatten(),
        keras.layers.Dense(128, activation=tf.nn.relu),
        keras.layers.Dropout(0.5),
        keras.layers.Dense(n, activation=tf.nn.softmax)
    ])

    model.compile(loss=keras.losses.categorical_crossentropy,
                  optimizer=keras.optimizers.Adadelta(),
                  metrics=['accuracy'])
    return model

def some_internet_model(n):
    model = keras.Sequential([
        keras.layers.Conv2D(200, (3, 3),
                            activation=tf.nn.relu,
                            input_shape=(28, 28, 1)),
        keras.layers.Conv2D(180, (3, 3),
                            activation=tf.nn.relu),
        keras.layers.MaxPooling2D(2, 2),
        keras.layers.Conv2D(180, (3, 3),
                            activation=tf.nn.relu),
        keras.layers.Conv2D(140, (3, 3),
                            activation=tf.nn.relu),
        keras.layers.Conv2D(100, (3, 3),
                            activation=tf.nn.relu),
        keras.layers.Conv2D(50, (3, 3),
                            activation=tf.nn.relu),
        keras.layers.MaxPooling2D(2, 2),
        keras.layers.Flatten(),
        keras.layers.Dense(180, activation=tf.nn.relu),
        keras.layers.Dense(100, activation=tf.nn.relu),
        keras.layers.Dense(50, activation=tf.nn.relu),
        keras.layers.Dropout(0.5),
        keras.layers.Dense(n, activation=tf.nn.softmax)
    ])
    model.compile(loss=keras.losses.categorical_crossentropy,
                  optimizer=keras.optimizers.Adam(lr=0.0001),
                  metrics=['accuracy'])

    return model

def keras_toy():
    fashion_mnist = keras.datasets.fashion_mnist
    (x_train, y_train), (x_test, y_test) = fashion_mnist.load_data()

    X_train = x_train.reshape(60_000, 28, 28, 1)
    X_test = x_test.reshape(10_000, 28, 28, 1)

    n_classes=10
    batch_size = 128
    epochs = 6

    y_train = keras.utils.to_categorical(y_train, n_classes)
    y_test = keras.utils.to_categorical(y_test, n_classes)

    model = cnn(n_classes)
    model.summary()

    callbacks = [
        keras.callbacks.EarlyStopping(patience=5, verbose=1),
        keras.callbacks.ReduceLROnPlateau(factor=0.1, patience=3, min_lr=0.001, verbose=1),
        keras.callbacks.ModelCheckpoint('model-minist.h5', verbose=1,
                                        save_best_only=True,
                                        save_weights_only=True)
    ]

    model.fit(X_train, y_train,
              batch_size=batch_size,
              epochs=epochs,
              callbacks=callbacks,
              verbose=1,
              validation_data=(X_test, y_test))
    score = model.evaluate(X_test, y_test, verbose=0)

    print(score)

    class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat',
                'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']


def main():
    parser = ap.ArgumentParser(description="Takes the inputs returned by convert-classified.sh to train a model")
    parser.add_argument('input_directory',
                        metavar='input_directory',
                        type=str,
                        help='Path to directory containing training inputs')
    args = parser.parse_args()

    input_directory = Path(args.input_directory)
    metadata_file = input_directory.joinpath("inputs.json")

    if not metadata_file.is_file():
        logger.error("%s does not exist, exiting", metadata_file)
        sys.exit(1)

    inputs = {}
    with open(metadata_file, 'r') as f:
        inputs = json.loads(f.read())

    if len(inputs.keys()) <= 0:
        logger.error("no inputs")
        sys.exit(1)

    first_X = np.load(input_directory.joinpath(next(iter(inputs.items()))[1]['X']))

    X = np.array([], dtype=np.uint8).reshape(0,first_X.shape[1])
    y = np.array([], dtype=np.uint8)

    for k, v in inputs.items():
        logger.info("Merging %s", k)
        X_file = input_directory.joinpath(v['X'])
        y_file = input_directory.joinpath(v['y'])

        if not X_file.is_file():
            logger.error("Missing file %s, skipping %s", X_file, k)
            continue

        if not y_file.is_file():
            logger.error("Missing file %s, skipping %s", y_file, k)
            continue

        _X = np.load(X_file)
        _y = np.load(y_file)

        X = np.vstack((X, _X))
        y = np.append(y, _y)

        logger.info("%s done merging", k)
        logger.info(X.shape)
        logger.info(y.shape)

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=42)
    model_X = X_train[:300_000]
    model_y = y_train[:300_000]

    logger.info(X_train.shape)


if __name__ == '__main__':
    keras_toy()
