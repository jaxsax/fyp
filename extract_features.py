#!/usr/bin/env python

from pathlib import Path
from shapely.geometry import mapping
from tqdm import tqdm
from rasterio.mask import mask
from rasterio.plot import reshape_as_image, reshape_as_raster, show

import argparse as ap
import rasterio as rio
import geopandas as gpd
import numpy as np
import matplotlib.pyplot as plt

import logging
import sys
import json

logging.basicConfig(level=logging.INFO,
                    format="%(asctime)s [%(levelname)s] %(message)s")

logger = logging.getLogger('extract_features')

def extension_in_list(arr, extension):
    matching = list(filter(lambda x: extension in x, arr))
    if len(matching) == 0:
        return None
    return matching[0]

def tqdm_enumerate(iter):
    i = 0
    for y in tqdm(iter):
        yield i, y
        i += 1

def main():
    parser = ap.ArgumentParser(description="Extracts features out of an shp file with a given stacked tif")
    parser.add_argument('input_directory',
                        metavar='input_directory',
                        type=str,
                        help='Path to directory containing shp related files')
    parser.add_argument('output_directory',
                        metavar='output_directory',
                        type=str,
                        help='Path to dump features')
    args = parser.parse_args()
    input_directory = Path(args.input_directory)

    if not input_directory.is_dir():
        logger.error("input_directory does not exist or is not a folder")
        sys.exit(1)

    files = [x.name for x in input_directory.iterdir()]

    shp_file = extension_in_list(files, ".shp")
    if shp_file is None:
        logger.error("input_directory does not contain a shp file")
        sys.exit(1)

    stacked_file = extension_in_list(files, "STACKED")
    if stacked_file is None:
        logger.error("input_directory does not contain stacked tif")
        sys.exit(1)

    output_directory = Path(args.output_directory)
    output_directory.mkdir(exist_ok=True)

    gt = gpd.read_file(input_directory.joinpath(shp_file))
    geometries = gt.geometry.values
    logger.info("%s contains %d features", shp_file, len(geometries))

    class_identifier_to_use = 'C_ID'
    if 'MC_ID' in gt:
        logger.info("Found MC_ID key, probably an SCP training input, setting class to MC_ID")
        class_identifier_to_use = 'MC_ID'

    with rio.open(input_directory.joinpath(stacked_file)) as src:
        band_count = src.count
        X = np.array([], dtype=rio.uint8).reshape(0, band_count)
        y = np.array([], dtype=rio.uint8)

        for index, geometry in tqdm_enumerate(geometries):
            feature = [mapping(geometry)]

            # if 'MC_info' in gt and gt['MC_info'][index] == 'Unclassified':
            #     logger.info("Skipping unclassified class")
            #     continue
            # elif gt['C_ID'][index] == 0:
            #     continue

            out_image, out_transform = mask(src, feature, crop=True)
            out_image_trimmed = out_image[:, ~np.all(out_image == 0, axis=0)]
            out_image_trimmed = out_image_trimmed[:, ~np.all(out_image_trimmed == 255, axis=0)]
            out_image_reshaped = out_image.reshape(-1, band_count)

            X = np.vstack((X, out_image_reshaped))
            y = np.append(y, [gt[class_identifier_to_use][index]] * out_image_reshaped.shape[0])

        train_X = output_directory.joinpath(f"X_{Path(shp_file).stem}.npy")
        train_y = output_directory.joinpath(f"y_{Path(shp_file).stem}.npy")

        np.save(train_X, X)
        np.save(train_y, y)

if __name__ == '__main__':
    main()
