#!/usr/bin/env python

from utils import create_band_sorter
from pathlib import Path
import argparse as ap
import rasterio as rio
import numpy as np

import sys
import logging

logging.basicConfig(level=logging.INFO,
                    format="%(asctime)s [%(name)s] [%(levelname)s] %(message)s")

logger = logging.getLogger('generate_stacked')

np.seterr(divide='ignore', invalid='ignore')

def get_files_containing_filter(directory, band_filter):
    return [x.name for x in directory.iterdir()
                    if any(True if b in x.name else False for b in band_filter)
            and (x.name.endswith(".tif") or x.name.endswith(".jp2")) ]

def ext_tif_or_jp2(directory):
    for x in directory.iterdir():
        if x.name.endswith(".tif"):
            return ".tif"
        elif x.name.endswith(".jp2"):
            return ".jp2"
    return None

def main():
    parser = ap.ArgumentParser(description="Generates a stacked image that contains all necessary bands")
    parser.add_argument('input_directory',
                        metavar='input_directory',
                        type=str,
                        help='Path to directory containing files with separated bands')
    parser.add_argument('output_file_path',
                        metavar='output_file_path',
                        type=str,
                        help='Path to output stacked file to')
    parser.add_argument('--bands',
                        type=str,
                        default="B02,B03,B04,B05,B06,B07,B08,B8A,B11,B12",
                        help='List of file suffixes to accept')
    args = parser.parse_args()

    input_directory = Path(args.input_directory)
    output_file_path = Path(args.output_file_path)
    bands_filter = [x for x in args.bands.strip().split(",")]

    # This is just here to provide a warning to the user in case they are using generating
    # stacked images that have inconsistent band counts
    # Here, 1 means we are inserting an NDVI layer
    number_of_bands_to_add = 1

    if output_file_path.is_file():
        wanted_band_counts = len(bands_filter) + number_of_bands_to_add
        with rio.open(output_file_path) as src:
            if src.count != wanted_band_counts:
                logger.warning("Inconsistent band counts, wanted=%d, got=%d; are you using an outdated stacked image?",
                               wanted_band_counts,
                               src.count)
        logger.info("Output file already exists, quitting")
        sys.exit(1)

    ext_to_use = ext_tif_or_jp2(input_directory)
    if ext_to_use is None:
        logger.error("Can't find jp2 or tif files in %s", input_directory)
        sys.exit(1)

    band_sorter = create_band_sorter(bands_filter, ext=ext_to_use)
    image_files = list(get_files_containing_filter(input_directory, bands_filter))
    image_files = sorted(image_files, key=band_sorter)

    first_file = input_directory.joinpath(image_files[0])
    with rio.open(first_file) as src:
        reference = src.profile
    reference.update(dtype=rio.uint8)
    reference.update(driver="GTiff")

    layers =[None]
    for idx, f in enumerate(image_files, start=1):
        logger.info("Reading band %s", f)
        with rio.open(input_directory.joinpath(f)) as src:
                data = src.read(1)
                data = (data * 255).round().astype(rio.uint8)
                layers.append(data)


    nir = layers[7]
    red = layers[3]
    ndvi = ((nir - red) / (nir + red)).astype(rio.uint8)

    layers.append(ndvi)
    selected_layers = layers

    # Minus 1 to ignore first layer
    stacked_layers_count = len(selected_layers) - 1
    reference.update(count=stacked_layers_count)
    reference.update(nodata=0)

    with rio.open(output_file_path, 'w', **reference) as dst:
        for idx, d in enumerate(selected_layers[1:], start=1):
            logger.info("Writing band %d", idx)
            dst.write_band(idx, d)

if __name__ == '__main__':
    main()
