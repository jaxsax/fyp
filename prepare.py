#!/usr/bin/env python

from pathlib import Path
from functools import partial, cmp_to_key
from rasterio.warp import reproject, Resampling

import rasterio as rio
import argparse as ap

import sys
import zipfile
import logging

logging.basicConfig(level=logging.INFO,
                    format="%(asctime)s [%(name)s] [%(levelname)s] %(message)s")

logger = logging.getLogger('prepare')

"""
This file takes as input a list of raw satellite images downloaded from Copernicus SciHub (zip files) and writes out the jp2 files
as tif files

Things it takes in:
    List of zip archives

Things it emits:
    bunch of tif files of all bands
"""

def extract_archive(input_directory, working_directory):
    logging.info("extracting all archives in %s to %s", input_directory, working_directory)
    if not input_directory.is_dir():
        return
    for f in input_directory.iterdir():
        if not zipfile.is_zipfile(f):
            continue
        zip_output_directory = working_directory.joinpath(f.stem)
        zip_SAFE_directory = zip_output_directory.joinpath(f"{f.stem}.SAFE")
        if zip_output_directory.is_dir():
            logger.info("%s already exists, skip unzipping", zip_output_directory)
            yield zip_SAFE_directory
            continue

        zip_output_directory.mkdir()
        with zipfile.ZipFile(f, "r") as zip_ref:
            logger.debug("unzipping %s to %s", f, zip_output_directory)
            zip_ref.extractall(zip_output_directory)
        yield zip_SAFE_directory

def write_to_tif(input_directory, output_directory):
    """
    This function writes jp2 files into GTiff files
    Expects input_directory to contain jp2 files
    """
    jp2_files = [x.name for x in input_directory.iterdir()
                 if x.name.endswith(".jp2")]
    for jp2 in jp2_files:
        input_file_path = input_directory.joinpath(jp2)
        output_file_path = output_directory.joinpath(f"{input_file_path.stem}.tif")
        with rio.open(input_file_path) as src:
            dst_profile=src.profile
            data = src.read(1)
        dst_profile.update(driver='GTiff')
        dst_profile.update(nodata=0)
        with rio.open(output_file_path, 'w', **dst_profile) as dst:
            dst.write_band(1, data)

def get_granule_jp2(granule_directory):
    for granule in [x for x in granule_directory.iterdir() if x.is_dir()]:
        img_directory = granule.joinpath("IMG_DATA")
        if img_directory.is_dir():
            yield (granule, img_directory)

def normalize_and_write_rgb(granule_directory, wanted_bands, output_directory):
    """
    Rewrites all images to match the same profile, this helps with
    identify features from shp; actually this might not be necessary, shp files are georeferenced no?
    """
    for granule in [x for x in granule_directory.iterdir() if x.is_dir()]:
        img_directory = granule.joinpath("IMG_DATA")
        warp = img_directory.joinpath("warp")
        warp.mkdir(exist_ok=True)
        jp2_files = [x.name for x in img_directory.iterdir()
                     if any(True if b in x.name else False for b in wanted_bands)
                     and x.name.endswith(".jp2")]
        sort_band = cmp_to_key(partial(_sort_band, bands=wanted_bands))
        jp2_files = sorted(jp2_files, key=sort_band)

        reference_profile=None
        with rio.open(img_directory.joinpath(jp2_files[2])) as src:
           reference_profile = src.profile
        reference_profile.update(driver='GTiff')
        reference_profile.update(count=len(jp2_files))

        with rio.Env():
            with rio.open(output_directory.joinpath(f"{granule.stem}_RGB.tif"), 'w', **reference_profile) as dst:
                for idx, f in enumerate(jp2_files, start=1):
                    logger.info("Warping %s", f)
                    file_name = img_directory.joinpath(f)
                    with rio.open(file_name) as src:
                        reproject(
                            source=rio.band(src, 1),
                            destination=rio.band(dst, idx),
                            src_transform=src.transform,
                            src_crs=src.crs,
                            src_nodata=src.nodata,
                            dst_transform=reference_profile['transform'],
                            dst_crs=reference_profile['crs'],
                            dst_nodata=reference_profile['nodata'],
                            resampling=Resampling.bilinear)


def main():
    parser = ap.ArgumentParser(description="Prepare sentinel images for training")
    parser.add_argument('input_directory',
                        metavar='input_directory',
                        type=str,
                        help='Path to directory containing satellite zip archives')
    parser.add_argument('working_directory',
                        metavar='working_directory',
                        type=str,
                        help='Temporary directory for intermediate files')
    parser.add_argument('output_directory',
                        metavar='output_directory',
                        type=str,
                        help='Path to output directory')
    args = parser.parse_args()

    input_directory = Path(args.input_directory)

    working_directory = Path(args.working_directory)
    working_directory.mkdir(exist_ok=True)

    output_directory = Path(args.output_directory)
    output_directory.mkdir(exist_ok=True)

    for archive_output in extract_archive(input_directory, working_directory):
        logger.info("Preprocessing %s", archive_output)
        granule_directory = archive_output.joinpath("GRANULE")

        for granule_path, image_directory in get_granule_jp2(granule_directory):
            output_granule_directory = output_directory.joinpath(granule_path.name)
            output_granule_directory.mkdir(exist_ok=True)
            write_to_tif(image_directory, output_granule_directory)

if __name__ == '__main__':
    main()
