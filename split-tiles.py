import rasterio as rio
from itertools import product
from rasterio import windows
import sys
from pathlib import Path

# https://gis.stackexchange.com/questions/285499/how-to-split-multiband-image-into-image-tiles-using-rasterio?noredirect=1&lq=1

def get_tiles(ds, width=256, height=256):
    nols, nrows = ds.meta['width'], ds.meta['height']
    offsets = product(range(0, nols, width), range(0, nrows, height))
    big_window = windows.Window(col_off=0, row_off=0, width=nols, height=nrows)
    for col_off, row_off in  offsets:
        window =windows.Window(col_off=col_off, row_off=row_off, width=width, height=height).intersection(big_window)
        transform = windows.transform(window, ds.transform)
        yield window, transform

def main(image_path):
    print(f"path: {image_path}")
    tile_out_directory=Path("processing").joinpath("tiles")
    tile_out_directory.mkdir(exist_ok=True)
    with rio.open(image_path) as src:
        tile_width , tile_height = 512, 512
        meta = src.meta.copy()

        for window, transform in get_tiles(src, tile_width, tile_height):
            meta['transform'] = transform
            meta['width'], meta['height'] = tile_width, tile_height
            output = tile_out_directory.joinpath(f"tile_{window.col_off}-{window.row_off}.tif")
            with rio.open(output, 'w', **meta) as dst:
                dst.write(src.read(window=window))


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("need image path")
        sys.exit(1)
    main(sys.argv[1])
