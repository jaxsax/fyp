#!/usr/bin/env python

"""
The purpose of this script is to have a single file that merges all the inputs and runs the training method
from all the other algorithms all at once generating accumulating performance metrics on the way
"""

from pathlib import Path
from sklearn.model_selection import train_test_split
from enum import Enum

import argparse as ap
import numpy as np
import sklearn.metrics as metrics
import sklearn.ensemble as ensemble
import sklearn.svm as svm
import sklearn.neural_network as neural_network
import sklearn.linear_model as linear_model
import sklearn.neighbors as neighbors
import sklearn.multiclass as multiclass
import train_dummy as dummy
import train_rf as rf
import train_kmeans as kmeans

import yaml
import logging
import xgboost
import lightgbm

logging.basicConfig(level=logging.INFO,
                    format="%(asctime)s [%(name)s] [%(levelname)s] %(message)s")

logger = logging.getLogger('train')

class Mode(Enum):
    CLASSIFIERS = 'classifiers'
    ENSEMBLE = 'ensemble'

    def __str__(self):
        return self.value

def main():
    parser = ap.ArgumentParser(description="Takes the inputs returned by convert-classified.sh to train all models")
    parser.add_argument('input_directory',
                        metavar='input_directory',
                        type=str,
                        help='Path to directory containing training inputs')
    parser.add_argument("results_file",
                        type=str,
                        help='Path to file to place results yaml')
    parser.add_argument("mode", type=Mode, choices=list(Mode))
    args = parser.parse_args()

    input_directory = Path(args.input_directory)
    results_file = Path(args.results_file)
    mode = args.mode
    metadata_file = input_directory.joinpath("inputs.yml")

    if not metadata_file.is_file():
        logger.error("%s does not exist, exiting", metadata_file)
        sys.exit(1)

    inputs = {}
    with open(metadata_file, 'r') as f:
        inputs = yaml.load(f.read())

    if len(inputs.keys()) <= 0:
        logger.error("no inputs")
        sys.exit(1)

    first_X = np.load(input_directory.joinpath(next(iter(inputs.items()))[1]['X']))

    X = np.array([], dtype=np.uint8).reshape(0,first_X.shape[1])
    y = np.array([], dtype=np.uint8)

    for k, v in inputs.items():
        logger.info("Merging %s", k)
        X_file = input_directory.joinpath(v['X'])
        y_file = input_directory.joinpath(v['y'])

        if not X_file.is_file():
            logger.error("Missing file %s, skipping %s", X_file, k)
            continue

        if not y_file.is_file():
            logger.error("Missing file %s, skipping %s", y_file, k)
            continue

        _X = np.load(X_file)
        _y = np.load(y_file)

        X = np.vstack((X, _X))
        y = np.append(y, _y)

        logger.info("Merged %s with shape X: %s, y: %s", k, _X.shape, _y.shape)

    logger.info("Total merged shape: X: %s y: %s", X.shape, y.shape)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=42)

    if mode == Mode.CLASSIFIERS:
        find_good_classifiers(X_train, X_test, y_train, y_test, results_file)
    elif mode == Mode.ENSEMBLE:
        test_ensemble_accuracy(X_train, X_test, y_train, y_test)

def test_ensemble_accuracy(X_train, X_test, y_train, y_test):
    classifiers = [
        ("randomforest", rf.create_classifier()),
        ("lgbm", lgbm())
        ("gb", gb())
    ]
    classifier = ensemble.VotingClassifier(estimators=classifiers, voting='hard')
    classifier.fit(X_train, y_train)
    y_pred = classifier.predict(X_test)

    results = create_score(y_test, y_pred)
    logger.info("ensemble accuracy kappa=%.2f", results['kappa'])
    logger.info(results)

def find_good_classifiers(X_train, X_test, y_train, y_test, results_file):
    classifiers = [
        # ("dummy", dummy.create_classifier()),
        ("randomforest", rf.create_classifier()),
        ("kNeighbors", kNN()),
        ("gb", gb()),
        ("lgbm", lgbm()),
        ("ovr", ovr())
        # ("kmeans", kmeans.create_classifier()),
        # ("linear_svc", linear_svc()),
        # ("mlp", mlp()),
        # ("sgd", sgd()),
        # ("perceptron", perceptron()),        
        # ("xgb", xgb()),
    ]

    results = {}
    for name, classifier in classifiers:
        logger.info("Running '%s'", name)
        classifier.fit(X_train, y_train)
        y_pred = classifier.predict(X_test)
        results[name] = create_score(y_test, y_pred)
        logger.info("%s done kappa=%.2f", name, results[name]['kappa'])
    
    with open(results_file, "w") as r:
        r.write(yaml.dump(results))

    results_sorted = sorted(results.items(), key=lambda x: x[1]['kappa'], reverse=True)
    best = max(results_sorted, key=lambda x: x[1]['kappa'])

    top_3, rest = results_sorted[:3], results_sorted[3:]

    logger.info('Keep the following')
    for t in top_3:
        logger.info("%s - kappa:%.2f", t[0], t[1]['kappa'])

    logger.info("Remove the following '%s'", ','.join(map(lambda x: x[0], rest)))

def create_score(y_test, y_pred):
    return {
        'accuracy': float(metrics.accuracy_score(y_test, y_pred)),
        'kappa': float(metrics.cohen_kappa_score(y_test, y_pred)),
        # 'au-roc': float(metrics.roc_auc_score(y_test, y_pred))
        # 'classification_report': metrics.classification_report(y_test, y_pred)
    }

def gb():
    return ensemble.GradientBoostingClassifier(random_state=42)

def linear_svc():
    return svm.LinearSVC(multi_class='ovr', random_state=42, max_iter=1000)

def mlp():
    return neural_network.MLPClassifier(random_state=42)

def sgd():
    return linear_model.SGDClassifier(n_jobs=-1, 
                                      random_state=42, 
                                      max_iter=1000,
                                      tol=1e-3)

def perceptron():
    return linear_model.Perceptron(n_jobs=-1, random_state=42, max_iter=1000, tol=1e-3)

def kNN():
    return neighbors.KNeighborsClassifier()

def xgb():
    return xgboost.XGBClassifier()

def lgbm():
    return lightgbm.LGBMClassifier()

def ovr():
    return multiclass.OneVsRestClassifier(estimator=lgbm())

if __name__ == "__main__":
    main()