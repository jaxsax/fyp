#!/usr/bin/env bash

INPUT_FOLDER=$1

if [ $# -lt 1 ]; then
    echo "Need arguments"
    exit 1;
fi

ls $INPUT_FOLDER

# ============== DUMMY =============

#echo "=================== TRAINING FOR BASELINE =================== "
#time python train_kmeans.py $INPUT_FOLDER

# ============== RANDOM FOREST =============

echo "=================== TRAINING FOR RANDOM FORESTS =================== "
time python train_rf.py $INPUT_FOLDER
