#!/usr/bin/env python

from pathlib import Path
from sklearn.model_selection import train_test_split
from sklearn.dummy import DummyClassifier

import argparse as ap
import numpy as np
import sklearn.metrics as metrics

import yaml
import logging

"""
Implements a baseline classifier to compare accuracies with other models
"""

logging.basicConfig(level=logging.INFO,
                    format="%(asctime)s [%(name)s] [%(levelname)s] %(message)s")

logger = logging.getLogger('train_dummy')

def main():
    parser = ap.ArgumentParser(description="Takes the inputs returned by convert-classified.sh to train a model")
    parser.add_argument('input_directory',
                        metavar='input_directory',
                        type=str,
                        help='Path to directory containing training inputs')
    args = parser.parse_args()

    input_directory = Path(args.input_directory)
    metadata_file = input_directory.joinpath("inputs.yml")

    if not metadata_file.is_file():
        logger.error("%s does not exist, exiting", metadata_file)
        sys.exit(1)

    inputs = {}
    with open(metadata_file, 'r') as f:
        inputs = yaml.load(f.read())

    if len(inputs.keys()) <= 0:
        logger.error("no inputs")
        sys.exit(1)

    first_X = np.load(input_directory.joinpath(next(iter(inputs.items()))[1]['X']))

    X = np.array([], dtype=np.uint8).reshape(0,first_X.shape[1])
    y = np.array([], dtype=np.uint8)

    for k, v in inputs.items():
        logger.info("Merging %s", k)
        X_file = input_directory.joinpath(v['X'])
        y_file = input_directory.joinpath(v['y'])

        if not X_file.is_file():
            logger.error("Missing file %s, skipping %s", X_file, k)
            continue

        if not y_file.is_file():
            logger.error("Missing file %s, skipping %s", y_file, k)
            continue

        _X = np.load(X_file)
        _y = np.load(y_file)

        X = np.vstack((X, _X))
        y = np.append(y, _y)

        logger.info("%s done merging", k)
        logger.info(X.shape)
        logger.info(y.shape)

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=42)
    model_X = X_train
    model_y = y_train

    classifier = DummyClassifier('most_frequent', random_state=42)
    classifier.fit(model_X, model_y)

    y_pred = classifier.predict(X_test)

    y_predl = change_numbers_to_labels(y_pred)
    y_testl = change_numbers_to_labels(y_test)

    accuracy = metrics.accuracy_score(y_testl, y_predl)
    report = metrics.classification_report(y_testl, y_predl)

    print(report)
    print("accuracy: {:0.3f}".format(accuracy))

def create_classifier():
    return DummyClassifier('most_frequent', random_state=42)

def number_to_label(v):
    if v == 0:
        return 'unclassified'
    elif v == 1:
        return 'water'
    elif v == 3:
        return 'artificial surfaces'
    elif v == 4:
        return 'agricultural area'
    elif v == 5:
        return 'mountains'
    elif v == 6:
        return 'forests'
    else:
        return 'unclassified'

def change_numbers_to_labels(arr):
    new_arr = []
    for v in arr:
        new_arr.append(number_to_label(v))
    return np.array(new_arr)

if __name__ == '__main__':
    main()
