#!/usr/bin/env python

from pathlib import Path
from sklearn.ensemble import RandomForestClassifier
from sklearn.externals import joblib
from sklearn.model_selection import train_test_split
from sklearn.tree import export_graphviz
from utils import batch_X_y
from tqdm import tqdm
from functools import reduce

import argparse as ap
import numpy as np
import sys
import logging
import json
import math
import rasterio as rio
import matplotlib.pyplot as plt

logging.basicConfig(level=logging.INFO,
                    format="%(asctime)s [%(name)s] [%(levelname)s] %(message)s")

logger = logging.getLogger('train_rf')

"""
This file manages taking the outputs of prepare.py, trains a model based on the inputs and saves a model to a specific file
"""

import sys

def generate_rf(X_train, y_train, n_trees=50, min_samples_leaf=1):
    rf = RandomForestClassifier(n_jobs=-1,
                                random_state=42,
                                n_estimators=n_trees,
                                min_samples_leaf=min_samples_leaf)
    rf.fit(X_train, y_train)

    return rf

def combine_rf(rf_a, rf_b):
    rf_a.estimators_ += rf_b.estimators_
    rf_a.n_estimators += len(rf_a.estimators_)
    return rf_a

def main(args):
    parser = ap.ArgumentParser(description="Takes the inputs returned by convert-classified.sh to train a model")
    parser.add_argument('input_directory',
                        metavar='input_directory',
                        type=str,
                        help='Path to directory containing training inputs')
    parser.add_argument('--search',
                        type=bool,
                        help='To run grid search or not',
                        default=False)

    args = parser.parse_args()

    input_directory = Path(args.input_directory)
    metadata_file = input_directory.joinpath("inputs.json")

    if not metadata_file.is_file():
        logger.error("%s does not exist, exiting", metadata_file)
        sys.exit(1)

    inputs = {}
    with open(metadata_file, 'r') as f:
        inputs = json.loads(f.read())

    if len(inputs.keys()) <= 0:
        logger.error("no inputs")
        sys.exit(1)

    first_X = np.load(input_directory.joinpath(next(iter(inputs.items()))[1]['X']))

    X = np.array([], dtype=np.uint8).reshape(0,first_X.shape[1])
    y = np.array([], dtype=np.uint8)

    for k, v in inputs.items():
        logger.info("Merging %s", k)
        X_file = input_directory.joinpath(v['X'])
        y_file = input_directory.joinpath(v['y'])

        if not X_file.is_file():
            logger.error("Missing file %s, skipping %s", X_file, k)
            continue

        if not y_file.is_file():
            logger.error("Missing file %s, skipping %s", y_file, k)
            continue

        _X = np.load(X_file)
        _y = np.load(y_file)

        X = np.vstack((X, _X))
        y = np.append(y, _y)

        logger.info("%s done merging", k)
        logger.info(X.shape)
        logger.info(y.shape)

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=42)
    if args.search:
        model_X = X_train[:100_000]
        model_y = y_train[:100_000]
        logger.info("X: %s y: %s", model_X.shape, model_y.shape)

        params = [(10, 5), (20, 5), (30, 5), (40, 5), (50, 5)]
        scores = []

        for param in params:
            rf = generate_rf(model_X, model_y, n_trees=param[0], min_samples_leaf=param[1])
            score = rf.score(X_test, y_test)
            scores.append(score)
            logger.info("%s %s", param, score)

        trees = [x[0] for x in params]
        plt.plot(trees, scores)
        plt.xticks(trees, [str(x) for x in trees])
        plt.show()
    else:
        best_n = (50, 5)
        logger.info("Generating random forest model on %s", best_n)

        rf = generate_rf(X_train, y_train,
                         n_trees=best_n[0],
                         min_samples_leaf=best_n[1])
        logger.info("Score: %f", rf.score(X_test, y_test))
        joblib.dump(rf, f"rf-{best_n[0]}-{best_n[1]}.pkl")

if __name__ == '__main__':
    main(sys.argv)
