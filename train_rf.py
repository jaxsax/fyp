#!/usr/bin/env python

from pathlib import Path
from sklearn.ensemble import RandomForestClassifier
from sklearn.externals import joblib
from sklearn.model_selection import train_test_split, RandomizedSearchCV
from sklearn.metrics import confusion_matrix
from sklearn.utils.multiclass import unique_labels
from sklearn.tree import export_graphviz
from utils import batch_X_y
from tqdm import tqdm
from functools import reduce

import argparse as ap
import numpy as np
import sklearn.metrics as metrics
import sys
import logging
import json
import math
import yaml
import utils as U
import rasterio as rio
import matplotlib.pyplot as plt

logging.basicConfig(level=logging.INFO,
                    format="%(asctime)s [%(name)s] [%(levelname)s] %(message)s")

logger = logging.getLogger('train_rf')

"""
This file manages taking the outputs of prepare.py, trains a model based on the inputs and saves a model to a specific file
"""

import sys

def generate_rf(X_train, y_train, n_trees=50, min_samples_leaf=1):
    rf = RandomForestClassifier(n_jobs=-1,
                                random_state=42,
                                n_estimators=n_trees,
                                max_depth=8,
                                min_samples_leaf=min_samples_leaf)
    rf.fit(X_train, y_train)

    return rf

def combine_rf(rf_a, rf_b):
    rf_a.estimators_ += rf_b.estimators_
    rf_a.n_estimators += len(rf_a.estimators_)
    return rf_a

def main(args):
    parser = ap.ArgumentParser(description="Takes the inputs returned by convert-classified.sh to train a model")
    parser.add_argument('input_directory',
                        metavar='input_directory',
                        type=str,
                        help='Path to directory containing training inputs')
    parser.add_argument('--search',
                        type=bool,
                        help='To run grid search or not',
                        default=False)

    args = parser.parse_args()

    input_directory = Path(args.input_directory)
    metadata_file = input_directory.joinpath("inputs.yml")

    if not metadata_file.is_file():
        logger.error("%s does not exist, exiting", metadata_file)
        sys.exit(1)

    inputs = {}
    with open(metadata_file, 'r') as f:
        inputs = yaml.load(f.read())

    if len(inputs.keys()) <= 0:
        logger.error("no inputs")
        sys.exit(1)

    first_X = np.load(input_directory.joinpath(next(iter(inputs.items()))[1]['X']))

    X = np.array([], dtype=np.uint8).reshape(0,first_X.shape[1])
    y = np.array([], dtype=np.uint8)

    for k, v in inputs.items():
        logger.info("Merging %s", k)
        X_file = input_directory.joinpath(v['X'])
        y_file = input_directory.joinpath(v['y'])

        if not X_file.is_file():
            logger.error("Missing file %s, skipping %s", X_file, k)
            continue

        if not y_file.is_file():
            logger.error("Missing file %s, skipping %s", y_file, k)
            continue

        _X = np.load(X_file)
        _y = np.load(y_file)

        X = np.vstack((X, _X))
        y = np.append(y, _y)

        logger.info("%s done merging", k)
        logger.info(X.shape)
        logger.info(y.shape)

    if args.search:
        model_X = X
        model_y = y
        logger.info("X: %s y: %s", model_X.shape, model_y.shape)

        param_grid = {'n_estimators': [100, 200, 300, 400, 500],
                      'min_samples_split': [2,4,6,8,10],
                      'min_samples_leaf': [2,4,6,8,10],
                      'max_depth': [6,8,10,12]}

        rf = RandomForestClassifier(oob_score=True, random_state=42)
        rs = RandomizedSearchCV(estimator=rf,
                                param_distributions=param_grid,
                                n_iter=10,
                                cv=5,
                                verbose=2,
                                n_jobs=-1,
                                random_state=42)

        results = rs.fit(model_X, model_y)
        print(rs.best_score_)
        print(rs.best_params_)
        joblib.dump(results.best_estimator_, "best-rf.pkl.compressed", compress=True)
    else:
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=42)
        old_params = {
            'oob_score': True,
            'n_estimators': 200,
            'min_samples_split': 8,
            'min_samples_leaf': 2,
            'random_state': 42,
            'max_depth': None,
            'n_jobs': -1
        }
        params = old_params.copy()
        params.update(n_estimators=300, min_samples_split=2, min_samples_leaf=2, max_depth=12)
        rf = RandomForestClassifier(**params)
        rf.fit(X_train, y_train)
        y_pred = rf.predict(X_test)
        joblib.dump(rf, "rf.pkl.compressed", compress=True)

        print(np.unique(y_test))

        y_predl = change_numbers_to_labels(y_pred)
        y_testl = change_numbers_to_labels(y_test)

        accuracy = metrics.accuracy_score(y_testl, y_predl)
        report = metrics.classification_report(y_testl, y_predl)

        print(report)
        print("accuracy: {:0.3f}".format(accuracy))

        b = "b02,b03,b04,b05,b06,b07,b08,b8a,b11,b12,ndvi".split(",")
        for b, imp in zip(b, rf.feature_importances_):
            print(f"{b} importance: {imp * 100}")

        plot_confusion_matrix(y_test, y_pred,
            np.array(['unclassified', 'water', None, 'artificial surfaces', 'agricultural area', 'mountains', 'forests']))
        plt.show()

        fig, ax = plt.subplots(1,3, figsize=[20,8])
        band_count = np.arange(1, 12)

        classes = np.unique(y_pred)
        for class_type in classes:
            band_intensity = np.mean(X_test[y_pred==class_type, :], axis=0)
            ax[0].plot(band_count, band_intensity, label=number_to_label(class_type))
            ax[1].plot(band_count, band_intensity, label=number_to_label(class_type))
            ax[2].plot(band_count, band_intensity, label=number_to_label(class_type))

        ax[0].set_xlabel("band #")
        ax[0].set_ylabel("reflectance value")
        ax[0].set_xticks(band_count)

        ax[1].set_xlabel("band #")
        ax[1].set_ylabel("reflectance value")
        ax[1].set_xticks(band_count)


        ax[1].set_ylim(10, 25)

        ax[2].set_xlabel("band #")
        ax[2].set_ylabel("reflectance value")
        ax[2].set_xticks(band_count)


        ax[2].set_ylim(30, 55)

        ax[1].legend(loc="upper right")
        ax[0].set_title('overall band intensity')
        ax[1].set_title('lower subset of band intensity')
        ax[2].set_title('upper subset of band intensity')

        plt.show()

def create_classifier():
    old_params = {
            'oob_score': True,
            'n_estimators': 200,
            'min_samples_split': 8,
            'min_samples_leaf': 2,
            'random_state': 42,
            'max_depth': None,
            'n_jobs': -1
    }
    params = old_params.copy()
    params.update(n_estimators=300, min_samples_split=2, min_samples_leaf=2, max_depth=12)
    return RandomForestClassifier(**params)

def number_to_label(v):
    if v == 0:
        return 'unclassified'
    elif v == 1:
        return 'water'
    elif v == 3:
        return 'artificial surfaces'
    elif v == 4:
        return 'agricultural area'
    elif v == 5:
        return 'mountains'
    elif v == 6:
        return 'forests'
    else:
        return 'unclassified'

def change_numbers_to_labels(arr):
    new_arr = []
    for v in arr:
        new_arr.append(number_to_label(v))
    return np.array(new_arr)

def plot_confusion_matrix(y_true, y_pred, classes,
                          normalize=False,
                          title=None,
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if not title:
        if normalize:
            title = 'Normalized confusion matrix'
        else:
            title = 'Confusion matrix, without normalization'

    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)
    # Only use the labels that appear in the data
    classes = classes[unique_labels(y_true, y_pred)]
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    return ax


if __name__ == '__main__':
    main(sys.argv)
