#!/usr/bin/env python

from pathlib import Path
from utils import sample_quota

import imblearn.under_sampling as under_sampling
import imblearn.over_sampling as over_sampling
import numpy as np

import sys
import collections

"""
Undersamples a set of feature+label set to its lowest feature
"""
def main():
    X_path = Path(sys.argv[1])
    y_path = Path(sys.argv[2])
    identifier = sys.argv[3]

    X = np.load(X_path)
    y = np.load(y_path)    

    new_X, new_y = oversample_SMOTE(X, y)
    print("Resampled %s" % collections.Counter(new_y))

    newX_path = f"out/X_{new_name(X_path, identifier)}.npy"
    newy_path = f"out/y_{new_name(y_path, identifier)}.npy"

    print(f"Saving X {newX_path}")
    np.save(newX_path, new_X)
    np.save(newy_path, new_y)

def oversample_SMOTE(X, y):
    smote = over_sampling.SMOTE()
    return smote.fit_resample(X, y)
    
def undersample_nearmiss(X, y):
    nm = under_sampling.NearMiss(version=3)
    return nm.fit_resample(X, y)

def undersample_rus(X, y):
    rus = under_sampling.RandomUnderSampler(random_state=42)
    return rus.fit_resample(X, y)

def resample_mask(X, y):
    mask = sample_quota(y, { 
        0: 5000,
        1: 120_000,
        3: 16509-8000,
        4: 5000+5000, 
        5: 5000-2000, 
        6: 5000+5000, 
        # 1: 35_000,
        # 3: 5000,
        # 4: 5000, 
        # 5: 6000, 
        # 6: 5000, 
    })
    X_resampled = X[mask]
    y_resampled = y[mask]

    return X_resampled, y_resampled
def new_name(path, n):
    """
    Removes the file's extension and 'X_' or 'y_' and appends a sampled to the file name
    """
    return f"sampled_{n}_{Path(path).stem[2:]}"

if __name__ == "__main__":
    main()