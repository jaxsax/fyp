import collections

import numpy as np
import matplotlib.pyplot as plt

from functools import partial, cmp_to_key
from itertools import product
from rasterio import windows
from sklearn.utils.multiclass import unique_labels
from sklearn.metrics import confusion_matrix


def get_files_matching(input_directory, wanted_bands, ext):
    return [x.name for x in input_directory.iterdir()
            if any([True if b in x.name else False for b in wanted_bands])
            and x.name.endswith(ext)]

def _sort_band(a, b, ext, bands):
    band_a = a[a.index(ext)-len(ext)+1:a.index(ext)]
    band_b = b[b.index(ext)-len(ext)+1:b.index(ext)]

    return bands.index(band_a) - bands.index(band_b)

def create_band_sorter(bands, ext):
    return cmp_to_key(partial(_sort_band, ext=ext, bands=bands))

def batch_train_test(X_train, y_train, X_test, y_test, batch_size=10_000):
    for start in range(0, len(X), batch_size):
        yield (X_train[start:start+batch_size], y_train[start:start+batch_size],
               X_test[start:start+batch_size], y_test[start:start+batch_size])

def batch_X_y(X, y, batch_size=10_000):
    if len(X) != len(y):
        return
    for start in range(0, len(X), batch_size):
        yield (X[start:start+batch_size], y[start:start+batch_size])

def batch(arr, batch_size=10_000):
    """
    This function returns a generator that 'batches' data from a list for feeding into ML algorithms.
    Reason for this is simple. My computer is really low specced and can't train the algorithm on a full set
    directly, it causes memory to swap really badly and slows my computer down to a crawl.

    Hopefully by batching it, it relieves the stress and allows for larger datasets to be trained albeit, slowly.
    """
    for start in range(0, len(arr), batch_size):
        yield arr[start:start+batch_size]

def get_tiles(ds, width=256, height=256):
    nols, nrows = ds.meta['width'], ds.meta['height']
    offsets = product(range(0, nols, width), range(0, nrows, height))
    big_window = windows.Window(col_off=0, row_off=0, width=nols, height=nrows)
    for col_off, row_off in  offsets:
        window =windows.Window(col_off=col_off, row_off=row_off, width=width, height=height).intersection(big_window)
        transform = windows.transform(window, ds.transform)
        yield window, transform

def sample_lowest(arr):
    """
    Returns a mask of values to accept sampling off the lowest label in the list
    """
    n = min(collections.Counter(arr).values())
    values = np.unique(arr)
    quota = dict(map(lambda x: (x, n), values))
    return sample_quota(arr, quota)

def sample_quota(arr, quota):
    values = np.unique(arr)
    values_added = dict(map(lambda x: (x, 0), values))
    mask = []
    for v in arr:
        if values_added[v] < quota[v]:
            mask.append(True)
            values_added[v] += 1
        else:
            mask.append(False)
    return np.array(mask)

def plot_confusion_matrix(y_true, y_pred, classes,
                          normalize=False,
                          title=None,
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if not title:
        if normalize:
            title = 'Normalized confusion matrix'
        else:
            title = 'Confusion matrix, without normalization'

    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)
    # Only use the labels that appear in the data
    classes = classes[unique_labels(y_true, y_pred)]
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    return ax
