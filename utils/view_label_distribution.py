#!/usr/bin/env python

from pathlib import Path

import numpy as np

import sys
import collections

def main():
    y_file = Path(sys.argv[1])

    y = np.load(y_file)

    print(collections.Counter(y))

if __name__ == '__main__' :
    main()
