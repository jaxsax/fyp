#!/usr/bin/env sh

from pathlib import Path

import geopandas as gpd

import sys

def main():
    shp_file = Path(sys.argv[1])
    gt = gpd.read_file(shp_file)
    df = gt.groupby('MC_info')['SCP_UID'].nunique()
    print(df)

if __name__ == '__main__':
    main()
