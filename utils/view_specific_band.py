#!/usr/bin/env python

from pathlib import Path
from rasterio.plot import show

import rasterio as rio

import sys

def main():
    img = Path(sys.argv[1])
    with rio.open(img) as src:
        data = src.read(11)
    show(data)

if __name__ == '__main__':
    main()
