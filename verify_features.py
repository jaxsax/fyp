#!/usr/bin/env python

import numpy as np

X = np.load("out/X_clip1_RT_T31TDH_20170413T104021.npy")
y = np.load("out/y_clip1_RT_T31TDH_20170413T104021.npy")

correct = X.shape[0] == y.shape[0]

print(X.shape)
print(y.shape)
print(f"correct? {'yes' if correct else 'no'}")
